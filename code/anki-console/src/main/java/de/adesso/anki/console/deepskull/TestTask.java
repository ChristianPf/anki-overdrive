package de.adesso.anki.console.deepskull;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.adesso.anki.Vehicle;
import de.adesso.anki.console.tasks.ConnectTask;
import de.adesso.anki.console.tasks.VehicleTask;
import de.adesso.anki.messages.LapTimeMessage;
import de.adesso.anki.roadmap.Roadmap;
import de.adesso.anki.roadmap.RoadmapScanner;
import de.adesso.anki.roadmap.roadpieces.Roadpiece;
import de.adesso.anki.roadmap.segments.Segment;
import de.adesso.anki.roadmap.segments.SegmentType;
import de.adesso.anki.sdk.messages.MessageListener;
import de.adesso.anki.messages.SegmentTimeMessage;

@Component
public class TestTask extends VehicleTask {
	protected Map<String, SegmentListener> listeners = new HashMap<>();
	
	@Autowired
	protected ConnectTask connect;

	@Override
	public String getCommand() {
		return "test2";
	}

	@Override
	public String getDescription() {
		return "simple test task";
	}

	@Override
	public void run(Vehicle car, String... args) {
		MapLearner maplearner = new MapLearner();
		car.addMessageListener(SegmentTimeMessage.class, msg -> {
			System.out.println("Position: " + msg.getSegmentPosInMap());
			System.out.println("Segmenttype:" + msg.getType());
			maplearner.newInfo(msg.getSegmentPosInMap(),msg.getType());
			SegmentType current = msg.getType();
			SegmentType next = maplearner.getSuccessorOf(msg.getSegmentPosInMap()).getType();
			
			
			System.out.println("Next should be: " + next);
		});
		
	}

}
