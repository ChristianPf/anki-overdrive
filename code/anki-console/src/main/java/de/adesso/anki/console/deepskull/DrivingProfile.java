package de.adesso.anki.console.deepskull;

import java.util.HashMap;

public class DrivingProfile {

	private HashMap<String,Integer> speedValues;
	private static boolean first = true;
	private static java.util.Random random;
	
	public DrivingProfile() {
		speedValues = new HashMap<>();
	}

	public Integer getSpeed(String parsingString) {
		return speedValues.getOrDefault(parsingString, 400);
	}
	
	public static DrivingProfile safeDefaults() {
		DrivingProfile result = new DrivingProfile();
		result.speedValues.put("ccc", 600);
		result.speedValues.put("ccs", 600);
		result.speedValues.put("csc", 700);
		result.speedValues.put("css", 700);
		result.speedValues.put("scc", 700);
		result.speedValues.put("scs", 700);
		result.speedValues.put("ssc", 1000);
		result.speedValues.put("sss", 1400);
		return result;
	}
	
	public static DrivingProfile dangerDefaults() {
		DrivingProfile result = new DrivingProfile();
		result.speedValues.put("ccc", 1000);
		result.speedValues.put("ccs", 1250);
		result.speedValues.put("csc", 1650);
		result.speedValues.put("css", 1550);
		result.speedValues.put("scc", 1000);
		result.speedValues.put("scs", 1400);
		result.speedValues.put("ssc", 1700);
		result.speedValues.put("sss", 2150);
		return result;
	}
	
	public DrivingProfile mutate() {
		if(first){
			random = new java.util.Random();
			first = false;
		}
		DrivingProfile result = clone();
		Integer raise = 10;
		for(String s: result.speedValues.keySet()) {
			if( random.nextFloat() < 0.5) {
			int newSpeed = result.speedValues.get(s)+raise;
			if(s.equals("ccc"))	result.speedValues.put(s, Math.min(newSpeed, 1000) );
			else if(s.equals("scc")) result.speedValues.put(s, Math.min(newSpeed, 1000));
			else result.speedValues.put(s, newSpeed);
			}
		}
		return result;
	}
	
	public DrivingProfile clone() {
		DrivingProfile result = new DrivingProfile();
		for(String s: speedValues.keySet())
			result.speedValues.put(s, this.speedValues.get(s));
		return result;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb = sb.append("ccc: ").append(speedValues.get("ccc")).append("\n");
		sb = sb.append("ccs: ").append(speedValues.get("ccs")).append("\n");
		sb = sb.append("csc: ").append(speedValues.get("csc")).append("\n");
		sb = sb.append("css: ").append(speedValues.get("css")).append("\n");
		sb = sb.append("scc: ").append(speedValues.get("scc")).append("\n");
		sb = sb.append("scs: ").append(speedValues.get("scs")).append("\n");
		sb = sb.append("ssc: ").append(speedValues.get("ssc")).append("\n");
		sb = sb.append("sss: ").append(speedValues.get("sss")).append("\n");
		return sb.toString();
	}
}
