package de.adesso.anki.console.deepskull;

import de.adesso.anki.messages.LapTimeMessage;
import de.adesso.anki.sdk.messages.MessageListener;

public class FastDriver implements MessageListener<LapTimeMessage> {

	private DrivingProfile profile, bestProfileSoFar;
	private long bestLapTime = 100000;
	
	public FastDriver() {
		System.out.println("Fast Driver starting with safe profile");
		//profile = DrivingProfile.safeDefaults();
		profile = DrivingProfile.dangerDefaults();
		bestProfileSoFar = profile;
		System.out.println("Fast Driver is ready.");
	}
	
	@Override
	public void messageReceived(LapTimeMessage msg) {
		long currentLapTime = msg.getLapTime();
		System.out.println("lap finished in "+currentLapTime);
		System.out.println("Previous best lap time: "+bestLapTime);
		if(currentLapTime < bestLapTime){
			System.out.println("New best lap time!!! :)");
			bestLapTime = currentLapTime;
			bestProfileSoFar = profile;
		} else {
			System.out.println("Not quite as good lap time :(");
		}
		System.out.println("Best Strategy so far:");
		System.out.println(bestProfileSoFar.toString());
		System.out.println("Mutating Driving Profile...");
		profile = bestProfileSoFar.mutate();
		System.out.println(profile.toString());
	}

	public int getSpeed(String parsingString) {
		return profile.getSpeed(parsingString);
	}
}
