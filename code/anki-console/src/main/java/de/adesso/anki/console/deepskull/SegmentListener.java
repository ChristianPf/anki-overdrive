package de.adesso.anki.console.deepskull;

import de.adesso.anki.sdk.messages.MessageListener;
import de.adesso.anki.messages.SegmentTimeMessage;

public class SegmentListener implements MessageListener<SegmentTimeMessage> {

	@Override
	public void messageReceived(SegmentTimeMessage arg0) {
		System.out.println("An Position " + arg0.getSegmentPosInMap() + " ist " + arg0.getType());
	}

}
