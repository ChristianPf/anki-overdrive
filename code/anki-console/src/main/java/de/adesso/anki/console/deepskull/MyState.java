package de.adesso.anki.console.deepskull;

import de.adesso.anki.learning.State;

public class MyState implements State {

	private String state;
	
	public MyState(String state){
		if(state.length() != 3)
			throw new RuntimeException("A state has to consist of three characters.");
		else {
			if( state.matches("[sc][sc][sc]") )
				this.state = state;
			else
				throw new RuntimeException("A state must consist only of the characters 'c' and 's'.");
		}
	}
			
	public String getState(){
		return state;
	}
			
	
	@Override
	public boolean isFinal() {
		return false;
	}

}
