package de.adesso.anki.console.deepskull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.adesso.anki.learning.Action;

public class MyAction implements Action {

	public static final Integer[] ALLSPEEDS = {400,600,800,1000,1200,1400,1600};
	public static final List<Integer> ALLSPEEDSLIST = Arrays.asList(ALLSPEEDS);
	
	private Integer speed;
	
	public MyAction(Integer speed){
		if( !ALLSPEEDSLIST.contains(speed))
			throw new RuntimeException("this speed is not allowed");
		this.setSpeed(speed);
	}

	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer speed) {
		if( ALLSPEEDSLIST.contains(speed))
			this.setSpeed(speed);
		else
			throw new RuntimeException("this speed is not allowed");
		
	}
	
	
}
