package de.adesso.anki.console.deepskull;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.adesso.anki.Vehicle;
import de.adesso.anki.console.tasks.ConnectTask;
import de.adesso.anki.console.tasks.VehicleTask;
import de.adesso.anki.messages.LapTimeMessage;
import de.adesso.anki.roadmap.Roadmap;
import de.adesso.anki.roadmap.RoadmapScanner;
import de.adesso.anki.roadmap.roadpieces.Roadpiece;
import de.adesso.anki.roadmap.segments.Segment;
import de.adesso.anki.roadmap.segments.SegmentType;
import de.adesso.anki.messages.SegmentTimeMessage;

@Component
public class BetterTask extends VehicleTask {
	protected Map<String, SegmentListener> listeners = new HashMap<>();
	
	@Autowired
	protected ConnectTask connect;

	@Override
	public String getCommand() {
		return "test";
	}

	@Override
	public String getDescription() {
		return "simple test task";
	}

	@Override
	public void run(Vehicle car, String... args) {
		car.addMessageListener(SegmentTimeMessage.class, msg -> {
			//System.out.println("Position: " + msg.getSegmentPosInMap());
			//System.out.println("Segmenttype:" + msg.getType());
			
			SegmentType current = msg.getType();
			String next = car.getCurrentSegment().getNext().getPiece().getType().toString();
			String nextNext = car.getCurrentSegment().getNext().getNext().getPiece().getType().toString();
			
			int triStraight = 100;
			int sthStraightCurve = 100;
			int straightCurveSth = 100;
			int straightCurveCurve = 100;
			int curveCurveCurve = 100;
			int initSpeed = 100;
			
			
			if(current.equals(SegmentType.STRAIGHT) && next.toLowerCase().contains("straight")) {
				car.setDesiredSpeed(triStraight);
			}
			if(next.toLowerCase().contains("straight") && nextNext.toLowerCase().contains("curve")){
				car.setDesiredSpeed(sthStraightCurve);
			}
			if(current.equals(SegmentType.STRAIGHT) && next.toLowerCase().contains("curve")) {
				car.setDesiredSpeed(straightCurveSth);
			}
			
			if(current.equals(SegmentType.STRAIGHT) && next.toLowerCase().contains("curve") && 
					nextNext.toLowerCase().contains("curve")){
				car.setDesiredSpeed(straightCurveCurve);
			}
			
			if((current.equals(SegmentType.CURVE_LEFT) || current.equals(SegmentType.CURVE_RIGHT)) && next.toLowerCase().contains("curve") && 
					nextNext.toLowerCase().contains("curve")){
				car.setDesiredSpeed(curveCurveCurve);
			}
		
		});
		
		car.setDesiredSpeed(300);
		
	}

}
