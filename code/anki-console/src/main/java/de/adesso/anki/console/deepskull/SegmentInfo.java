package de.adesso.anki.console.deepskull;

import de.adesso.anki.roadmap.segments.SegmentType;

public class SegmentInfo {

	private int position = 0;
	private SegmentType type = SegmentType.CROSSROADS;
	
	public SegmentInfo(int position) {
		this.position = position; 
	}
	
	public SegmentType getType() {
		return type;
	}
	public void setType(SegmentType segmentType) {
		this.type = segmentType;
	}
	public int getPosition() {
		return position;
	}
	
}
