package de.adesso.anki.console.deepskull;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.adesso.anki.Vehicle;
import de.adesso.anki.console.tasks.ConnectTask;
import de.adesso.anki.console.tasks.VehicleTask;
import de.adesso.anki.messages.LapTimeMessage;
import de.adesso.anki.roadmap.Roadmap;
import de.adesso.anki.roadmap.RoadmapScanner;
import de.adesso.anki.roadmap.roadpieces.Roadpiece;
import de.adesso.anki.roadmap.segments.Segment;
import de.adesso.anki.roadmap.segments.SegmentType;
import de.adesso.anki.messages.SegmentTimeMessage;

@Component
public class FastTask extends VehicleTask {
	protected Map<String, SegmentListener> listeners = new HashMap<>();
	
	@Autowired
	protected ConnectTask connect;

	@Override
	public String getCommand() {
		return "fast";
	}

	@Override
	public String getDescription() {
		return "learning by doing";
	}

	@Override
	public void run(Vehicle car, String... args) {
		// generate first driving profile
		FastDriver driver = new FastDriver();
		car.addMessageListener(LapTimeMessage.class, driver);
		
		//DrivingProfile profile = DrivingProfile.safeDefaults();
		//DrivingProfile bestProfileSoFar = profile;
		//long bestLapTime = 100000;
		
		//car.addMessageListener(LapTimeMessage.class, msg -> {
		//	long currentLapTime = msg.getLapTime();
		//	if(currentLapTime < bestLapTime)
		//		bestProfileSoFar = profile;
		//});
		
		
		// Drive according to DrivingProfile profile
		car.addMessageListener(SegmentTimeMessage.class, msg -> {
			SegmentType current = msg.getType();
			String next = car.getCurrentSegment().getNext().getPiece().getType().toString();
			String nextNext = car.getCurrentSegment().getNext().getNext().getPiece().getType().toString();
			
			String parsingString = "";
			if(current.toString().toLowerCase().contains("curve"))
				parsingString += "c";
			else 
				parsingString += "s";
			if(next.toLowerCase().contains("curve"))
				parsingString += "c";
			else 
				parsingString += "s";
			if(nextNext.toLowerCase().contains("curve"))
				parsingString += "c";
			else 
				parsingString += "s";
			
			//fetch speed from driving profile 
			int speed = driver.getSpeed(parsingString);
			car.setDesiredSpeed(speed);
			System.out.println("Road situation: " + parsingString + " with speed " + speed);
		});
		
		car.setDesiredSpeed(600);
		
	}

}
