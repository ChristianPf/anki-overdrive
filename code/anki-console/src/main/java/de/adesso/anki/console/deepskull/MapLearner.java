package de.adesso.anki.console.deepskull;

import java.util.ArrayList;

import de.adesso.anki.roadmap.segments.SegmentType;

public class MapLearner {

	SegmentInfo[] segmentInfos = new SegmentInfo[20];
	int highestSegmentSeen = 0;
	
	public MapLearner() {
		for( int i = 0; i< segmentInfos.length; i++)
			segmentInfos[i] = new SegmentInfo(i);
	}
	
	public void newInfo( int segmentPosition, SegmentType segmentType) {
		segmentInfos[segmentPosition].setType(segmentType);
		highestSegmentSeen = Math.max(highestSegmentSeen, segmentPosition);
	}
	
	public SegmentInfo getSegment(int i) {
		return segmentInfos[i];
	}
	
	public SegmentInfo getSuccessorOf(int i){
		if(i == highestSegmentSeen)
			return segmentInfos[0];
		return segmentInfos[i+1];
	}
}
